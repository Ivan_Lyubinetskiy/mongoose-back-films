const mongoose = require('mongoose');

const Actor = mongoose.model('Actor', {
    name: {
      type: String,
      required: true,
      // validate: {
      //   validator: function(name) {
      //     return !(name.trim() == "");
      //   }
      // }
    },
    age: {
      type: Number,
      required: true
    }

});

module.exports = { Actor };


